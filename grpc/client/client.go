package client

import (
	"gitlab.com/market2075343/sale_service/config"
	"gitlab.com/market2075343/sale_service/genproto/branch_service"
	"gitlab.com/market2075343/sale_service/genproto/product_service"
	"gitlab.com/market2075343/sale_service/genproto/staff_service"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type ServiceManagerI interface {
	// Product Service
	ProductService() product_service.ProductServiceClient
	CategoryService() product_service.CategoryServiceClient

	// Branch Service
	BranchService() branch_service.BranchServiceClient
	BranchProductService() branch_service.BranchProductServiceClient

	// Staff Service
	StaffTariffService() staff_service.StaffTariffServiceClient
	StaffService() staff_service.StaffServiceClient
}

type grpcClients struct {
	// Product Service
	productService  product_service.ProductServiceClient
	categoryService product_service.CategoryServiceClient

	// Branch Service
	branchService        branch_service.BranchServiceClient
	branchProductService branch_service.BranchProductServiceClient

	// Staff Service
	staffTariffService staff_service.StaffTariffServiceClient
	staffService       staff_service.StaffServiceClient
}

func NewGrpcClients(cfg config.Config) (ServiceManagerI, error) {
	// Product Microservice
	connProductService, err := grpc.Dial(
		cfg.ProductServiceHost+cfg.ProductGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	// Branch Microservice
	connBranchService, err := grpc.Dial(
		cfg.BranchServiceHost+cfg.BranchGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	// Staff Microservice
	connStaffService, err := grpc.Dial(
		cfg.StaffServiceHost+cfg.StaffGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	return &grpcClients{
		// Product Service
		productService:  product_service.NewProductServiceClient(connProductService),
		categoryService: product_service.NewCategoryServiceClient(connProductService),

		// Branch Service
		branchService:        branch_service.NewBranchServiceClient(connBranchService),
		branchProductService: branch_service.NewBranchProductServiceClient(connBranchService),

		// Staff Service
		staffTariffService: staff_service.NewStaffTariffServiceClient(connStaffService),
		staffService:       staff_service.NewStaffServiceClient(connStaffService),
	}, nil
}

// Product Service
func (g *grpcClients) ProductService() product_service.ProductServiceClient {
	return g.productService
}

func (g *grpcClients) CategoryService() product_service.CategoryServiceClient {
	return g.categoryService
}

// Branch Service
func (g *grpcClients) BranchService() branch_service.BranchServiceClient {
	return g.branchService
}

func (g *grpcClients) BranchProductService() branch_service.BranchProductServiceClient {
	return g.branchProductService
}

// Staff Service
func (g *grpcClients) StaffTariffService() staff_service.StaffTariffServiceClient {
	return g.staffTariffService
}

func (g *grpcClients) StaffService() staff_service.StaffServiceClient {
	return g.staffService
}
