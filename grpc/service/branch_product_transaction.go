package service

import (
	"context"

	"gitlab.com/market2075343/sale_service/config"
	"gitlab.com/market2075343/sale_service/genproto/sale_service"
	"gitlab.com/market2075343/sale_service/grpc/client"
	"gitlab.com/market2075343/sale_service/pkg/logger"
	"gitlab.com/market2075343/sale_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type BranchProductTransactionService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*sale_service.UnimplementedBrPrTransactionServiceServer
}

func NewBranchProductTransactionService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *BranchProductTransactionService {
	return &BranchProductTransactionService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (u *BranchProductTransactionService) Create(ctx context.Context, req *sale_service.BrPrTrCreateReq) (*sale_service.BrPrTrCreateResp, error) {
	u.log.Info("====== BranchProductTransaction Create ======", logger.Any("req", req))

	resp, err := u.strg.BranchProductTransaction().Create(ctx, req)
	if err != nil {
		u.log.Error("error while creating branch_product_transaction", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *BranchProductTransactionService) GetList(ctx context.Context, req *sale_service.BrPrTrGetListReq) (*sale_service.BrPrTrGetListResp, error) {
	u.log.Info("====== BranchProductTransaction GetList ======", logger.Any("req", req))

	resp, err := u.strg.BranchProductTransaction().GetList(ctx, req)
	if err != nil {
		u.log.Error("error while getting branch_product_transactions", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *BranchProductTransactionService) GetById(ctx context.Context, req *sale_service.BrPrTrIdReq) (*sale_service.BrPrTransaction, error) {
	u.log.Info("====== BranchProductTransaction GetById ======", logger.Any("req", req))

	resp, err := u.strg.BranchProductTransaction().GetById(ctx, req)
	if err != nil {
		u.log.Error("error while getting branch_product_transaction", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *BranchProductTransactionService) Update(ctx context.Context, req *sale_service.BrPrTrUpdateReq) (*sale_service.BrPrTrUpdateResp, error) {
	u.log.Info("====== BranchProductTransaction Update ======", logger.Any("req", req))

	resp, err := u.strg.BranchProductTransaction().Update(ctx, req)
	if err != nil {
		u.log.Error("error while updating branch_product_transaction", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *BranchProductTransactionService) Delete(ctx context.Context, req *sale_service.BrPrTrIdReq) (*sale_service.BrPrTrDeleteResp, error) {
	u.log.Info("====== BranchProductTransaction Delete ======", logger.Any("req", req))

	resp, err := u.strg.BranchProductTransaction().Delete(ctx, req)
	if err != nil {
		u.log.Error("error while deleting branch_product_transaction", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}
