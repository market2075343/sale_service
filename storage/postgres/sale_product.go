package postgres

import (
	"context"
	"fmt"

	"github.com/jackc/pgx"
	"github.com/jackc/pgx/v5/pgxpool"
	"gitlab.com/market2075343/sale_service/genproto/sale_service"
)

type SaleProductRepo struct {
	db *pgxpool.Pool
}

func NewSaleProductRepo(db *pgxpool.Pool) *SaleProductRepo {
	return &SaleProductRepo{
		db: db,
	}
}

// func (r *SaleProductRepo) Create(ctx context.Context, req *sale_service.SaleProductCreateReq) (*sale_service.SaleProductCreateResp, error) {
// 	query := `
// 	INSERT INTO sale_products(
//         sale_id,
//         product_id,
// 		quantity,
// 		price
// 	)
// 	VALUES($1,$2,$3,$4);
// 	`

// 	_, err := r.db.Exec(ctx, query,
// 		req.SaleId,
// 		req.ProductId,
// 		req.Quantity,
// 		req.Price,
// 	)

// 	if err != nil {
// 		fmt.Println("error:", err.Error())
// 		return nil, err
// 	}

// 	return &sale_service.SaleProductCreateResp{
// 		Msg: "OK",
// 	}, nil
// }

func (r *SaleProductRepo) GetList(ctx context.Context, req *sale_service.SaleProductGetListReq) (*sale_service.SaleProductGetListResp, error) {
	var (
		filter  = " WHERE TRUE "
		offsetQ = " OFFSET 0;"
		limit   = " LIMIT 10 "
		offset  = (req.Page - 1) * req.Limit
		count   int
	)

	s := `
	SELECT 
		sale_id,
		product_id,
		quantity,
		price
	FROM sale_products `

	if req.Limit > 0 {
		limit = fmt.Sprintf("LIMIT %d", req.Limit)
	}
	if offset > 0 {
		offsetQ = fmt.Sprintf("OFFSET %d", offset)
	}
	if req.SaleId != "" {
		filter += ` AND sale_id = '` + req.SaleId + `' `
	}

	query := s + filter + limit + offsetQ

	countS := `SELECT COUNT(*) FROM sale_products` + filter

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	if err = r.db.QueryRow(ctx, countS).Scan(&count); err != nil {
		return nil, err
	}

	resp := &sale_service.SaleProductGetListResp{}
	for rows.Next() {
		var saleProduct = sale_service.SaleProduct{}
		err := rows.Scan(
			&saleProduct.SaleId,
			&saleProduct.ProductId,
			&saleProduct.Quantity,
			&saleProduct.Price,
		)

		if err != nil {
			return nil, err
		}
		resp.SaleProducts = append(resp.SaleProducts, &saleProduct)
		resp.Count = int64(count)
	}

	return resp, nil
}

func (r *SaleProductRepo) GetById(ctx context.Context, req *sale_service.SaleProductIdReq) (*sale_service.SaleProduct, error) {
	query := `
    SELECT 
        sale_id,
        product_id,
        quantity,
        price
    FROM sale_products
    WHERE id=$1;
    `

	var saleProduct = sale_service.SaleProduct{}
	if err := r.db.QueryRow(ctx, query, req.SaleId).Scan(
		&saleProduct.SaleId,
		&saleProduct.ProductId,
		&saleProduct.Quantity,
		&saleProduct.Price,
	); err != nil {
		return nil, err
	}

	return &saleProduct, nil
}

func (r *SaleProductRepo) Update(ctx context.Context, req *sale_service.SaleProductUpdateReq) (*sale_service.SaleProductUpdateResp, error) {
	query := `
    UPDATE sale_products 
    SET 
        product_id=$2,
        quantity=$3,
        price=$4
    WHERE sale_id=$1;`

	res, err := r.db.Exec(ctx, query,
		req.SaleId,
		req.ProductId,
		req.Quantity,
		req.Price,
	)

	if err != nil {
		return nil, err
	}
	if res.RowsAffected() == 0 {
		return nil, pgx.ErrNoRows
	}

	return &sale_service.SaleProductUpdateResp{Msg: "OK"}, nil
}

func (r *SaleProductRepo) Delete(ctx context.Context, req *sale_service.SaleProductIdReq) (*sale_service.SaleProductDeleteResp, error) {
	query := `
	DELETE FROM 
		sale_products
	WHERE sale_id=$1;`

	res, err := r.db.Exec(ctx, query,
		req.SaleId,
	)

	if err != nil {
		return nil, err
	}
	if res.RowsAffected() == 0 {
		return nil, pgx.ErrNoRows
	}

	return &sale_service.SaleProductDeleteResp{Msg: "OK"}, nil
}

func (r *SaleProductRepo) Create(ctx context.Context, req *sale_service.SaleProductCreateReq) (*sale_service.SaleProductCreateResp, error) {
	updateQuery := `
	UPDATE sale_products AS sp
	SET quantity = sp.quantity + $3, price = sp.price + $4
	WHERE sale_id=$1 AND product_id=$2;`

	_, err := r.db.Exec(ctx, updateQuery, req.SaleId, req.ProductId, req.Quantity, req.Price)
	if err != nil {
		return nil, err
	}

	insertQuery := `
	INSERT INTO sale_products (sale_id, product_id, quantity, price)
    	SELECT $1, $2, $3, $4
        WHERE NOT EXISTS (
            SELECT 1 FROM sale_products WHERE sale_id = $1 AND product_id = $2
        )
	`

	_, err = r.db.Exec(ctx, insertQuery, req.SaleId, req.ProductId, req.Quantity, req.Price)
	if err != nil {
		return nil, err
	}

	return &sale_service.SaleProductCreateResp{Msg: "success"}, nil
}

func (r *SaleProductRepo) Count(ctx context.Context, saleId, productId string) (int, error) {
	var countQ, count int
	countS := `
	SELECT COUNT(*) FROM sale_products
	WHERE sale_id=$1 AND product_id=$2;`

	err := r.db.QueryRow(ctx, countS, saleId, productId).Scan(&countQ)
	if err != nil {
		return 0, err
	}

	query := `
	SELECT 
		quantity
	FROM sale_products
	WHERE sale_id = $1 AND product_id = $2;`

	if countQ > 0 {
		err := r.db.QueryRow(ctx, query, saleId, productId).Scan(&count)
		if err != nil {
			return 0, err
		}

		return count, nil
	}

	return 0, nil
}
