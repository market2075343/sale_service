package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx"
	"github.com/jackc/pgx/v5/pgxpool"
	"gitlab.com/market2075343/sale_service/genproto/sale_service"
)

type SaleRepo struct {
	db *pgxpool.Pool
}

func NewSaleRepo(db *pgxpool.Pool) *SaleRepo {
	return &SaleRepo{
		db: db,
	}
}

func (r *SaleRepo) Create(ctx context.Context, req *sale_service.SaleCreateReq) (*sale_service.SaleCreateResp, error) {
	id := uuid.NewString()

	query := `
	INSERT INTO sales(
		id,
		branch_id,
		cashier_id,
		price
	)
	VALUES($1,$2,$3,$4);
	`

	_, err := r.db.Exec(ctx, query,
		id,
		req.BranchId,
		req.CashierId,
		req.Price,
	)

	if err != nil {
		fmt.Println("error:", err.Error())
		return nil, err
	}

	return &sale_service.SaleCreateResp{
		Msg: id,
	}, nil
}

func (r *SaleRepo) GetList(ctx context.Context, req *sale_service.SaleGetListReq) (*sale_service.SaleGetListResp, error) {
	var (
		filter      = " WHERE deleted_at IS NULL "
		offsetQ     = " OFFSET 0;"
		limit       = " LIMIT 10 "
		offset      = (req.Page - 1) * req.Limit
		count       int
		paymentType sql.NullString
		status      sql.NullString
		clientName  sql.NullString
	)

	s := `
	SELECT 
		id,
		branch_id,
		cashier_id,
		price,
		payment_type,
		status,
		client_name,
		created_at::TEXT,
		updated_at::TEXT 
	FROM sales `

	if req.ClientName != "" {
		filter += ` AND name ILIKE ` + "'%" + req.ClientName + "%' OR address ILIKE '%" + req.ClientName + "%' "
	}
	if req.BranchId != "" {
		filter += ` AND branch_id='` + req.BranchId + `' `
	}
	if req.CashierId != "" {
		filter += ` AND cashier_id='` + req.CashierId + `' `
	}
	if req.PaymentType != "" {
		filter += ` AND payment_type='` + req.PaymentType + `' `
	}
	if req.Status != "" {
		filter += ` AND status='` + req.Status + `' `
	}

	filter += ` AND price BETWEEN ` + fmt.Sprintf("%f", req.PriceFrom) + ` AND ` + fmt.Sprintf("%f", req.PriceTo)

	filter += ` AND created_at BETWEEN '` + req.CreatedAtFrom + `' AND '` + req.CreatedAtTo + `' `

	if req.Limit > 0 {
		limit = fmt.Sprintf("LIMIT %d", req.Limit)
	}
	if offset > 0 {
		offsetQ = fmt.Sprintf("OFFSET %d", offset)
	}

	query := s + filter + limit + offsetQ

	countS := `SELECT COUNT(*) FROM sales` + filter

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	err = r.db.QueryRow(ctx, countS).Scan(&count)
	if err != nil {
		return nil, err
	}

	resp := &sale_service.SaleGetListResp{}
	for rows.Next() {
		var sale = sale_service.Sale{}
		err := rows.Scan(
			&sale.Id,
			&sale.BranchId,
			&sale.CashierId,
			&sale.Price,
			&paymentType,
			&status,
			&clientName,
			&sale.CreatedAt,
			&sale.UpdatedAt,
		)

		if err != nil {
			return nil, err
		}

		sale.PaymentType = paymentType.String
		sale.Status = status.String
		sale.ClientName = clientName.String

		resp.Sales = append(resp.Sales, &sale)
		resp.Count = int64(count)
	}

	return resp, nil
}

func (r *SaleRepo) GetById(ctx context.Context, req *sale_service.SaleIdReq) (*sale_service.Sale, error) {
	query := `
	SELECT 
		id,
		branch_id,
		cashier_id,
		price,
		payment_type,
		status,
		client_name,
		created_at::TEXT,
		updated_at::TEXT
	FROM sales
	WHERE id=$1 AND deleted_at IS NULL;
	`

	var (
		sale        = sale_service.Sale{}
		paymentType sql.NullString
		status      sql.NullString
		clientName  sql.NullString
	)
	if err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&sale.Id,
		&sale.BranchId,
		&sale.CashierId,
		&sale.Price,
		&paymentType,
		&status,
		&clientName,
		&sale.CreatedAt,
		&sale.UpdatedAt,
	); err != nil {
		return nil, err
	}
	sale.PaymentType = paymentType.String
	sale.Status = status.String
	sale.ClientName = clientName.String

	return &sale, nil
}

func (r *SaleRepo) Update(ctx context.Context, req *sale_service.SaleUpdateReq) (*sale_service.SaleUpdateResp, error) {
	query := `
	UPDATE sales 
	SET 
		branch_id=$2,
		cashier_id=$3,
		price=$4,
		payment_type=$5,
		status=$6,
		client_name=$7,
		updated_at=NOW()
	WHERE id=$1;`

	res, err := r.db.Exec(ctx, query,
		req.Id,
		req.BranchId,
		req.CashierId,
		req.Price,
		req.PaymentType,
		req.Status,
		req.ClientName,
	)

	if err != nil {
		return nil, err
	}
	if res.RowsAffected() == 0 {
		return nil, pgx.ErrNoRows
	}

	return &sale_service.SaleUpdateResp{Msg: "OK"}, nil
}

func (r *SaleRepo) Delete(ctx context.Context, req *sale_service.SaleIdReq) (*sale_service.SaleDeleteResp, error) {
	query := `
    UPDATE sales 
    SET 
        deleted_at=NOW() 
    WHERE id=$1;`

	res, err := r.db.Exec(ctx, query,
		req.Id,
	)

	if err != nil {
		return nil, err
	}
	if res.RowsAffected() == 0 {
		return nil, pgx.ErrNoRows
	}

	return &sale_service.SaleDeleteResp{Msg: "OK"}, nil
}
